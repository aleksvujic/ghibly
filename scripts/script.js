$(document).ready(function() {
	const gifDuration = 6000;
	
	setTimeout(function() {
	    $('#loading').css('display', 'none');
	    $('#iframeContent').removeAttr('style');
	}, gifDuration);
});